
// global variables
var GRAVITY = 9.80665;

var LeftRightAngle;
var FrontBackAngle;

var LRevents;
var FBevents;

var rotateLR;
var rotateFB;

// image dimensions
var axisWidth = 602;
var axisHeight = 325;

var plumbWidth = 49;
var plumbHeight = 278; // height of visual plumb-line (half of total image height)

var scaling = 1.0;

var layoutWidth;
var layoutHeight;
var fullLayoutHeight;

var plumbBackWidth;
var plumbBackHeight;

// SmoothOver functions and variable
var eventsMax = 3;

function addTo(eventArray, eventItem) {
	if (eventArray.length == eventsMax) {
		eventArray.shift();
	}
	eventArray.push(eventItem);
}

function getAvg(eventArray) {
	var sum = 0;
	var avg = 0;
	if (eventArray.length > 0) {
		for (i=0; i<eventArray.length; i++) {
			sum += eventArray[i];
		}
		avg = sum / eventArray.length;
	}
	return avg;
}

//layout function to handle screen orientation and scaling for different screen sizes
function adjustLayout() {
	
	var scalingWidth;
	var scalingHeight;
	var plumbBackTop;
	var plumbBackLeft;
	
	// get width and height of screen (modified for margins and text-height)
	var windowWidth = window.innerWidth - 20;
	var windowHeight = window.innerHeight - 80;
	
	if (windowWidth > windowHeight) {
		// set landscape properties (float left on .layout)
		document.getElementById('layoutLeftRight').style.float = 'left';
		document.getElementById('layoutFrontBack').style.float = 'left';
		
		layoutWidth = parseInt(windowWidth / 2);
		layoutHeight = windowHeight;
	} else {
		// set portrait properties (no float on .layout)
		document.getElementById('layoutLeftRight').style.float = 'none';
		document.getElementById('layoutFrontBack').style.float = 'none';
		
		layoutWidth = windowWidth;
		layoutHeight = parseInt(windowHeight / 2);
	}
	
	// compare layout-size with axis-image-size to calculate scaling
	scalingWidth = layoutWidth / axisWidth;
	scalingHeight = layoutHeight / axisHeight;
	// scale to the smaller of the two values
	if (scalingWidth < scalingHeight) {
		scaling = scalingWidth;
	} else {
		scaling = scalingHeight;
	}
	// don't get more than 2x if scaling up (prevent images from looking too ugly)
	if (scaling > 2) {
		scaling = 2.0;
	}
	
	// adjust layouts accordingly
	layoutWidth = parseInt(axisWidth * scaling);
	layoutHeight = parseInt(axisHeight * scaling);
	fullLayoutHeight = parseInt(layoutHeight + 40);
	
	// adjust the layout sizes accordingly
	document.getElementById('layoutLeftRight').style.width = layoutWidth + 'px';
	document.getElementById('layoutLeftRight').style.height = fullLayoutHeight + 'px';
	document.getElementById('layoutFrontBack').style.width = layoutWidth + 'px';
	document.getElementById('layoutFrontBack').style.height = fullLayoutHeight + 'px';
	
	// adjust background image-sizes
	document.getElementById('axesLeftRight').style.backgroundSize = layoutWidth + 'px ' + layoutHeight + 'px';
	document.getElementById('axesFrontBack').style.backgroundSize = layoutWidth + 'px ' + layoutHeight + 'px';
	
	// adjust the plumb-line image sizes accordingly
	plumbBackWidth = parseInt(plumbWidth * scaling);
	plumbBackHeight = parseInt(plumbHeight * scaling * 2);
	plumbBackTop = parseInt((plumbHeight * scaling * (-1)) + (plumbBackWidth / 2));
	plumbBackLeft = parseInt((layoutWidth / 2) - (plumbBackWidth / 2));

	document.getElementById('plumbLeftRight').style.width = plumbBackWidth + 'px';
	document.getElementById('plumbLeftRight').style.height = plumbBackHeight + 'px';
	document.getElementById('plumbFrontBack').style.width = plumbBackWidth + 'px';
	document.getElementById('plumbFrontBack').style.height = plumbBackHeight + 'px';
	
	document.getElementById('plumbLeftRight').style.backgroundSize = plumbBackWidth + 'px ' + plumbBackHeight + 'px';
	document.getElementById('plumbFrontBack').style.backgroundSize = plumbBackWidth + 'px ' + plumbBackHeight + 'px';
	
	document.getElementById('plumbLeftRight').style.left = plumbBackLeft + 'px';
	document.getElementById('plumbLeftRight').style.top = plumbBackTop + 'px';
	document.getElementById('plumbFrontBack').style.left =  plumbBackLeft + 'px';
	document.getElementById('plumbFrontBack').style.top =  plumbBackTop + 'px';
	
}

// PlumLine image transformation
function rotateImage(imageElem, angle) {
	
	if (typeof document.documentElement.style.mozTransform == "string") {
		// mozilla
		document.getElementById(imageElem).style.mozTransform = 'rotate('+angle+'deg)';
		
	} else if (typeof document.documentElement.style.oTransform  == "string") {
		// opera
		document.getElementById(imageElem).style.oTransform = 'rotate('+angle+'deg)';
		
	} else if (typeof document.documentElement.style.msTransform == "string") {
		// ie
		document.getElementById(imageElem).style.msTransform = 'rotate('+angle+'deg)';
		
	} else if (typeof document.documentElement.style.webkitTransform == "string") {
		// webkit (anticipated default on android browser)
		document.getElementById(imageElem).style.webkitTransform = 'rotate('+angle+'deg)';
		
	} else {
		// default CSS3
		document.getElementById(imageElem).style.transform = 'rotate('+angle+'deg)';
	}
	
}

window.onorientationchange = function() {
	setTimeout(adjustLayout, 1000);
};

//wait for PhoneGap to load
document.addEventListener("deviceready", loaded, false);

// PhoneGap is ready
function loaded() {
	// the Text Views from the Java Source
	LeftRightAngle = document.getElementById('angleLeftRight');
	FrontBackAngle = document.getElementById('angleFrontBack');
	
	LRevents = new Array();
	FBevents = new Array();
	
	adjustLayout();
	startWatch();
}
// Start watching the acceleration
function startWatch() {
	// Update acceleration 5 times every second
	var options = { frequency: 200 };
	watchID = navigator.accelerometer.watchAcceleration(onSuccess, onError, options);
}
// Stop watching the acceleration
function stopWatch() {
	if (watchID) {
		navigator.accelerometer.clearWatch(watchID);
		watchID = null;
	}
}
// Success
function onSuccess(acceleration) {
	
	addTo(LRevents, acceleration.x);
	// left-to-right
	rotateLR = parseInt((Math.round( Math.acos(getAvg(LRevents) / GRAVITY) * 180 / Math.PI)) - 90);
	// error-correction
	if (isNaN(rotateLR)) {
		rotateLR = -90;
	}
	rotateLR *= (-1);
	if (rotateLR > 0 && acceleration.x < 0) {
		rotateLR *= (-1);
	}
	// fill the text field
	LeftRightAngle.innerHTML = rotateLR + ' degrees';
	// calculating how to draw the image
	rotateImage('plumbLeftRight', rotateLR);
	
	addTo(FBevents, acceleration.z);
	// front-to-back
	rotateFB = parseInt(Math.round( Math.acos(getAvg(FBevents) / GRAVITY) * 180 / Math.PI) - 90);
	// error-correction
	if (isNaN(rotateFB)) {
		rotateFB = -90;
	}
    // fill the text field
	FrontBackAngle.innerHTML = rotateFB + ' degrees';
	// calculating how to draw the image
	rotateImage('plumbFrontBack', rotateFB);
	
}
 // Error
function onError() {
	alert('onError!');
}

