/**
 * Class SmoothOver
 * A simple smoothing algorithm to reduce the
 * amount of erratic jittering of the plumb-line.
 */

package au.edu.flinders.good0262.plumblinephonegap;

import java.util.ArrayList;

public class SmoothOver {
	int size;
	ArrayList<Float> events;
	
	public SmoothOver() {
		// smaller sizes increase the jittering of the plumb.
		// larger sizes (20, 10, 5) slow down the responsiveness
		// and give inaccurate results, especially at the edges.
		this.size = 3;
		this.events = new ArrayList<Float>(this.size);
	}
	
	/**
	 * add new values to the Array, popping off the oldest value
	 * @param e
	 */
	public void addTo(Float e) {
		if (this.events.size() == this.size) {
			for (int i=1; i<this.size; i++) {
				this.events.set(i-1, this.events.get(i));
			}
			this.events.set(this.size - 1, e);
		} else {
			this.events.add(e);
		}
	}
	
	/**
	 * get average of values in array
	 * @return avg
	 */
	public float getAvg() {
		float sum = 0;
		float avg = 0;
		if (this.events.size() > 0) {
			for (int i=0; i<this.events.size(); i++) {
				sum += this.events.get(i);
			}
			avg = sum / this.events.size();
		}
		return avg;
	}

}
