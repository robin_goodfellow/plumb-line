package au.edu.flinders.good0262.plumblinephonegap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import au.edu.flinders.good0262.plumblinephonegap.R;
import au.edu.flinders.good0262.plumblinephonegap.SmoothOver;
import org.apache.cordova.*;

public class PlumbLine extends DroidGap implements SensorEventListener {
	
	SensorManager sensorManager = null;
	
	TextView LeftRightAngle;
	TextView FrontBackAngle;
	
	ImageView imageLeftRight;
	ImageView imageFrontBack;
	
	Bitmap bitmapAxis;
	Bitmap bitmapPlumb;
	
	SmoothOver LRevents;
	SmoothOver FBevents;
	
	Float LRavg;
	Float FBavg;
	
	Float rotateLR;
	Float rotateFB;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.plumb_hanging);
        super.loadUrl("file:///android_asset/www/index.html");
        
        /*
        // the Text Views in the Activity View
        LeftRightAngle = (TextView) findViewById(R.id.angleLeftRight);
        FrontBackAngle = (TextView) findViewById(R.id.angleFrontBack);
        
        // the Images in the Activity View
        imageLeftRight = (ImageView) findViewById(R.id.imageLeftRight);
        imageFrontBack = (ImageView) findViewById(R.id.imageFrontBack);
        
        // the Bitmaps of the SourceImages
        bitmapPlumb = BitmapFactory.decodeResource(getResources(), R.drawable.plumb_line_plumb);
        bitmapAxis = BitmapFactory.decodeResource(getResources(), R.drawable.plumb_line_axes);
        
        //the smoothing-over of the values
        LRevents = new SmoothOver();
    	FBevents = new SmoothOver();
    	*/
    	
        // start the SensorManager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        
    }
    
    @Override
    protected void onResume() {
       super.onResume();
       sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY), SensorManager.SENSOR_DELAY_NORMAL);
    }
	
    @Override
    protected void onStop() {
       super.onStop();
       sensorManager.unregisterListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.plumb_hanging, menu);
        return true;
    }
    
	public void onAccuracyChanged(Sensor sensor, int accuracy) { }
	
	
	public void onSensorChanged(SensorEvent event) {
		/*
		synchronized (this) {
            switch (event.sensor.getType()){
                case Sensor.TYPE_GRAVITY:
                	// left-to-right
                	LRevents.addTo(event.values[0]);
                	rotateLR = Float.valueOf((Math.round( Math.acos(LRevents.getAvg() / SensorManager.GRAVITY_EARTH) * 180 / Math.PI)) - 90);
                	rotateLR *= (-1);
                	// error-correction
                	if (rotateLR > 0 && event.values[0] < 0) {
                		rotateLR *= (-1);
                	}
                	// fill the text field
                	LeftRightAngle.setText(rotateLR + " degrees");
                	// calculating how to draw the image
                	rotateImage(imageLeftRight, rotateLR);
                	
                	// front-to-back
                	FBevents.addTo(event.values[2]);
                	rotateFB = Float.valueOf(Math.round( Math.acos(FBevents.getAvg() / SensorManager.GRAVITY_EARTH) * 180 / Math.PI) - 90);
                    // fill the text field
                	FrontBackAngle.setText(rotateFB + " degrees");
                	// calculating how to draw the image
                	rotateImage(imageFrontBack, rotateFB);
                	
                    break;
            }
		}
		*/
	}
	
	/**
	 * rotateImage
	 * Rotates the bitmap in imageview by angle degrees
	 * @param imageview
	 * @param angle
	 */
	private void rotateImage(ImageView imageview, Float angle) {
		
        // Properties of the Images        
		int plumbWidth = bitmapPlumb.getWidth();
		int axisWidth = bitmapAxis.getWidth();
		
		// find the necessary details
		Float plumbCenter = (float) plumbWidth / 2;
		Float axisCenter = (float) axisWidth / 2;
		Float startPlumb = (float) axisCenter - plumbCenter;
		
		// set up the matrices
		Matrix matrix = new Matrix();
		Matrix matrixr = new Matrix();
		Matrix matrixt = new Matrix();
		
		// rotate the image then translate it to the centre
		matrixr.postRotate(angle, plumbCenter, 0);
		matrixt.postTranslate(startPlumb, plumbCenter);
		matrix.setConcat(matrixt, matrixr);
		
		// set the image to the matrix
		imageview.setImageMatrix(matrix);
	}
	
}
